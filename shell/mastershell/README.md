# Sobre o Master$hell

---------------------------------------------------
 Instruções
---------------------------------------------------
- Estando no diretório do mastershell/, execute:
	- Para compilar: make
	- Para executar: make run
- Importante: executar 'export LD_LIBRARY_PATH=.' se deseja que a lib seja buscada no diretorio atual (do mastershell)

---------------------------------------------------
 Funções Implementadas
---------------------------------------------------
- Execução de comandos internos 'quit' ou 'exit', 'help', 'cd', 'printlogo', 'ljobs'
- Execução de comandos externos via fork() e exec() utilizando o caminho de busca (PATH) herdado do shell padrão (bash)
- Os comandos externos aceitam uma lista variável de argumentos
- Redirecionamento de entrada: leitura (<)
- Redirecionamento de saída: sobrescrita (>), concatenação (>>)
- O comando 'exit' ou 'quit' causam o término do programa com return EXIT_SUCCESS
- Execução de processos via pipeline
- Execução de processos (jobs) em foreground e background
- Listagem de processos com status e ground type

---------------------------------------------------
 Sobre o código
---------------------------------------------------
- Foi utilizado o modelo do shell Foosh disponibilizado pelo professor.
- A implementação das funcionalidades do shell foram feitas dentro do arquivo main.c na função main()
- Foi criada uma função execute_pipeline() em main.c que faz a execução recursiva de
  comandos em pipeline.
- A estrutura pipeline_t definida em mastershell.h foi alterada com a adicao de uma variável
  'int redirect' para suportar o comando ">>" de append e junto a isso, novas macros foram definidas
  em mastershell.h
- Uma pequena alteração na função 'find_modifiers()' em parser.c foi necessária também para o suporte
  ao comando ">>" de append

---------------------------------------------------
Grupo:
---------------------------------------------------
- Guilherme Augusto Bileki - 4287189
- Luis Gustavo B. Ferrassini - 7987414
- Rodrigo das Neves Bernardi - 8066395